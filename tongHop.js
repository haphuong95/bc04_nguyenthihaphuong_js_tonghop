// EX_1: in bảng số từ 1 đến 100
function inBang() {
  var contentHTML = "";
  for (var i = 1; i <= 100; i += 10) {
    for (var j = i; j <= i + 9; j++) {
      contentHTML += j + ` <span class="p-3"></span>`;
    }
    contentHTML += `</br>`;
  }
  document.getElementById("ex_1").innerHTML = contentHTML;
}

// EX_2: tìm và in ra các số nguyên tố trong mảng user nhập
// tạo mảng số nguyên N
var numArr = [];
function themSoN() {
  var soNguyenN = document.getElementById("so-nguyen-n").value * 1;
  document.getElementById("so-nguyen-n").value = "";

  numArr.push(soNguyenN);
  document.getElementById("day_so_n").innerHTML = `${numArr}`;
}

//  kiểm tra và in ra số nguyên tố trong mảng
function soNguyenTo() {
  function isprime(n) {
    //flag = 0 => không phải số nguyên tố
    //flag = 1 => số nguyên tố

    var flag = 1;

    if (n < 2)
      return (flag = 0); /*Số nhỏ hơn 2 không phải số nguyên tố => trả về 0*/

    /*Sử dụng vòng lặp while để kiểm tra có tồn tại ước số nào khác không*/
    var i = 2;
    while (i < n) {
      if (n % i == 0) {
        flag = 0;
        break; /*Chỉ cần tìm thấy 1 ước số là đủ và thoát vòng lặp*/
      }
      i++;
    }

    return flag;
  }

  var result = [];

  /*Tìm  các số nguyên tố trong mảng*/
  for (var i = 0; i < numArr.length; i++) {
    if (isprime(numArr[i]) == 1) {
      result.push(numArr[i]);
    }
  }

  document.getElementById(
    "ex_2"
  ).innerHTML = `<h3 class="py-4"> ${result}</h3>`;
}

// EX_3: tính S=(2+3+4+...+n)+2n
function tinhTong() {
  var soN = document.getElementById("so-n").value * 1;
  var sum = 0;
  var result;
  if (soN < 2) {
    result = 0;
  } else {
    for (var i = 2; i <= soN; i++) {
      // biến đếm là i
      sum += i;
    }
    result = sum + 2 * soN;
  }
  document.getElementById(
    "ex_3"
  ).innerHTML = `<h3 class="py-4"> ${result}</h3>`;
}

// EX_4: tính ước số của N
function tinhUoc() {
  var soN = document.getElementById("so-N").value * 1;
  var count = 0;
  var result = "";
  for (var i = 1; i <= soN; i++) {
    // biến đếm là i
    if (soN % i == 0) {
      result += soN / i + ",";
      count++;
    }
  }
  document.getElementById("ex_4").innerHTML = `<h3 class="py-4">
  Ước số của N: ${result} </br>
  Số lượng ước số của N: ${count} </br>
  </h3>`;
}

// EX_5: tìm số đảo ngược của số nguyên dương N
function soDaoNguoc() {
  var soN = document.getElementById("so-nd-n").value;
  function reverseString(str) {
    // chuyển 1 chuỗi sang 1 mảng
    var arrayOfChars = str.split("");

    // đảo ngược các phần tử trong mảng
    var strOfArray = arrayOfChars.reverse();

    // gộp các phần tử của mảng thành 1 chuỗi
    var newString = strOfArray.join("");

    // trả về 1 chuỗi đã được đảo ngược
    return newString;
  }
  reverseString(soN);

  document.getElementById("ex_5").innerHTML = `<h3 class="py-4">${reverseString(
    soN
  )}</h3>`;
}

// EX_6: tìm x nguyên dương lớn nhất
function timX() {
  var i = 0;
  // biến đếm là i
  var sum = 0;
  while (sum < 100) {
    i++;
    sum += i;
  }
  i -= 1;
  document.getElementById("ex_6").innerHTML = `<h3 class="py-4"> ${i}</h3>`;
}

// EX_7: in bảng cửu chương
function bangCuuChuong() {
  var soX = document.getElementById("so-X").value * 1;
  var contentHTML = "";

  for (var i = 0; i <= 10; i++) {
    contentHTML += `<h3>${soX} x ${i} = ${soX * i} </h3>`;
  }
  document.getElementById("ex_7").innerHTML = contentHTML;
}

// EX_8: chi bài cho 4 người chơi
var player_1 = [];
var player_2 = [];
var player_3 = [];
var player_4 = [];
function chiaBai() {
  var cards = [
    "4K",
    "KH",
    "5C",
    "KA",
    "QH",
    "KD",
    "2H",
    "10S",
    "AS",
    "7H",
    "9K",
    "10D",
  ];

  for (var index = 0; index < cards.length; index++) {
    var currentCard = cards[index];
    if (index == 0 || index == 4 || index == 8) {
      player_1.push(currentCard);
    } else if (index == 1 || index == 5 || index == 9) {
      player_2.push(currentCard);
    } else if (index == 2 || index == 6 || index == 10) {
      player_3.push(currentCard);
    } else {
      player_4.push(currentCard);
    }
  }
  document.getElementById("ex_8").innerHTML = `<h3 class="py-4">
  Player 1: ${player_1} </br>
  Player 2: ${player_2} </br>
  Player 3: ${player_3} </br>
  Player 4: ${player_4} 
  </h3>`;
}

// EX_9: tìm số gà, số chó
function tinhGaCho() {
  var tongM = document.getElementById("tong-m").value * 1;
  var tongN = document.getElementById("tong-n").value * 1;
  var soGa = 0;
  var soCho = 0;
  // giải hệ phương trình, (soGa + soCho = tongM) và (2*soGa + 4*soCho = tongN)
  soCho = (tongN - 2 * tongM) / 2;
  soGa = tongM - soCho;

  document.getElementById("ex_9").innerHTML = `<h3 class="py-4">
  Số con gà: ${soGa} </br>
  Số con chó: ${soCho} </br>
  </h3>`;
}

// EX_10: tính góc lệch giữa kim giờ và kim phút
function gocLech() {
  var kimGio = document.getElementById("so-gio").value * 1;
  var kimPhut = document.getElementById("so-phut").value * 1;
  var gocLech;
  document.getElementById(
    "ex_10"
  ).innerHTML = `<h3 class="py-4">Góc lệch: ${gocLech}</h3>`;
}
